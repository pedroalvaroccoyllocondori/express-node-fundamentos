const mongoose = require('mongoose')//e hacen las  inportaciones con comillas simples
const { mongodb } = require('./config')

const connection = mongoose.connect(`mongodb://${mongodb.host}:${mongodb.port}/${mongodb.database}`)
.then((db)=>{
    console.log('Conexión exitosa en mongo bd')
}).catch((err)=>{
    console.log('Ha ocurrido un error: ' + err)
})

module.exports = connection