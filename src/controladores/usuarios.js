const Usuario = require('../modelos/usuarios')
const Usuarios = require('../modelos/usuarios')


const obtenerUsuarios=(solicitud,respuesta)=>{
    Usuarios.find({},(error,resultado)=>{
        if (error) {
            console.log("ha ocurrido un error")
        }else{
            respuesta.render('todos',{usuarios:resultado})//valores enviados al formulario
        }
    })
}

const obtenerCrearUsuarios=(solicitud,respuesta)=>{
    respuesta.render('crear')
}

const obtenerActualizarUsuarios=(solicitud,respuesta)=>{
    const parametro= solicitud.params.id
    Usuario.find({_id:parametro},(error,resultado)=>{
        if (error) {
            console.log('ha ocurrido un error'+error)
        }else{
            console.log(resultado)
            respuesta.render('actualizar',{usuarios:resultado})//valores enviados al formulario

        }
    })
    console.log(parametro)
}

const obtenerQuitarUsuarios=(solicitud,respuesta)=>{
    const parametro= solicitud.params.id
    Usuario.find({_id:parametro},(error,resultado)=>{
        if (error) {
            console.log('ha ocurrido un error'+error)
        }else{
            console.log(resultado)
            respuesta.render('quitar',{usuarios:resultado})//valores enviados al formulario

        }
    })
   
}

const crearUsuarios=(solicitud,respuesta)=>{
    
    const data = solicitud.body
    const user = new Usuario({
        nombre: data.nombre,
        edad:data.edad
    })
    user.save((error,resultado)=>{
        if (error) {
            console.log("ha ocurrido un error al enviar los datos" +error)
        }else{
            console.log("usuario registrado")
            respuesta.redirect("/usuarios/todos")
        }

    })

}

const actualizarUsuarios=(solicitud,respuesta)=>{

    const parametro = solicitud.params.id
    const data= solicitud.body
    Usuario.findOneAndUpdate({_id:parametro},data,(error,resultado)=>{
        if (error) {
            console.log('ha ocurrido un error'+ error)

        }else{
            console.log("usuario actualiza")
            respuesta.redirect('/usuarios/todos')
        }

    })
    console.log(parametro)
    

}

const quitarUsuarios=(solicitud,respuesta)=>{
    const parametro = solicitud.params.id
    Usuario.deleteOne({_id:parametro},(error,resultado)=>{
        if (error) {
            console.log('ha ocurrido un error'+ error)

        }else{
            console.log("usuario eliminado")
            respuesta.redirect('/usuarios/todos')
        }

    })
    console.log(parametro)
    
}


module.exports={
    obtenerUsuarios,
    obtenerCrearUsuarios,
    obtenerActualizarUsuarios,
    obtenerQuitarUsuarios,
    crearUsuarios,
    actualizarUsuarios,
    quitarUsuarios

}