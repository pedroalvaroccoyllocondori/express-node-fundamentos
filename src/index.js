const  express=require('express')

const usuarios=require('./rutas/usuarios')
const middlewareRegistro=require('./middleware/registro')
const path =require('path')
const conexion = require('./connection')
const applicacion=express()


//configuraciones
applicacion.set('titulo','aplicacion hecha en node')
applicacion.set('puerto',3000)
applicacion.set('view engine','ejs')                             //configuracion tiene q ser hecha en ingles
applicacion.set('views',path.join(__dirname,'vistas'))          //configuracion tiene q ser hecha en ingles
//usando middleware
//usando  la ruta para servir los archivos estaticos
//applicacion.use(middlewareRegistro.estarRegistrado)
applicacion.use(express.static(path.join(__dirname,'public')))
applicacion.use(express.urlencoded({extended:false}))

//rutas iniciales
applicacion.get('/',(solicitud,respuesta)=>{
    respuesta.render('index')
})

applicacion.use("/usuarios",usuarios)            //colocamos para q inicie desde esa ruta "usuarios" y de baya a los subrutas             //lo hacemos para indicarle q use las rutas de usuarios


applicacion.listen(applicacion.get('puerto'),()=>{
    console.log('mi '+applicacion.get('titulo')+' esta en el puerto'+applicacion.get('puerto'))
})