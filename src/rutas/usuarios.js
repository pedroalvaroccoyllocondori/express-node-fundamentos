const express=require('express')
const rutas= express.Router()
const usuariosControladores=require("../controladores/usuarios")

//metodo get
rutas.get('/todos',usuariosControladores.obtenerUsuarios)

rutas.get('/crear',usuariosControladores.obtenerCrearUsuarios)

rutas.get('/actualizar/:id',usuariosControladores.obtenerActualizarUsuarios)//mandamos el parametro id

rutas.get('/quitar/:id',usuariosControladores.obtenerQuitarUsuarios)//mandamos el parametro id

//metodo post
rutas.post('/crear',usuariosControladores.crearUsuarios)

//metodo put
rutas.post('/actualizar/:id',usuariosControladores.actualizarUsuarios)//mandamos el parametro id y ponemos el metodo POST

//metodo delete

rutas.post('/quitar/:id',usuariosControladores.quitarUsuarios)//mandamos el parametro id y ponemos el metodo POST

module.exports=rutas