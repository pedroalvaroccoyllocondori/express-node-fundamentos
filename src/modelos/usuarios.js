const mongoose = require('mongoose')
const { mongodb } = require('../config')
const Schema=mongoose.Schema

const UsuarioEsquema = new Schema({
    nombre:String,
    edad:Number
})

const Usuario=mongoose.model('Usuario',UsuarioEsquema)

module.exports=Usuario