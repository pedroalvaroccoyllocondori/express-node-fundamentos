module.exports = {
     misql_database :{
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'my_base_datos'
    },
    mongodb:{
        host: 'localhost',
        user: '',
        password: '',
        database: 'my_base_datos_mongo',
        port:27017
    }
}